﻿
using System.Collections.Generic;


using BusinessLogic.InterfacesFizzBuzz;
using BusinessLogic.ClassesFizzBuzz;

using Moq;
using NUnit.Framework;



namespace FizzBuzzUnitTest
{
    [TestFixture]
    public class FizzBuzzServiceUnitTest
    {
        private IFizzBuzzService fizzbuzzservice;
        List<string> finallist = new List<string>(new string[] {"1", "2", "fizz", "4", "buzz","fizz", "7", "8", "fizz", "buzz", 
            "11", "fizz", "13", "14", "fizzbuzz" });
        [SetUp]
        public void Loadcontext()
        {
            //Arrange
            var mockdivisibleby = new Mock<IDivisible>();

            mockdivisibleby.Setup(p => p.Check(It.IsAny<int>())).Returns(true);
            mockdivisibleby.Setup(p => p.GetOutput(It.Is<int>(i => i == 1))).Returns("1");
            mockdivisibleby.Setup(p => p.GetOutput(It.Is<int>(i => i == 2))).Returns("2");
            mockdivisibleby.Setup(p => p.GetOutput(It.Is<int>(i => i == 3))).Returns("fizz");
            mockdivisibleby.Setup(p => p.GetOutput(It.Is<int>(i => i == 4))).Returns("4");
            mockdivisibleby.Setup(p => p.GetOutput(It.Is<int>(i => i == 5))).Returns("buzz");
            mockdivisibleby.Setup(p => p.GetOutput(It.Is<int>(i => i == 6))).Returns("fizz");
            mockdivisibleby.Setup(p => p.GetOutput(It.Is<int>(i => i == 7))).Returns("7");
            mockdivisibleby.Setup(p => p.GetOutput(It.Is<int>(i => i == 8))).Returns("8");
            mockdivisibleby.Setup(p => p.GetOutput(It.Is<int>(i => i == 9))).Returns("fizz");
            mockdivisibleby.Setup(p => p.GetOutput(It.Is<int>(i => i == 10))).Returns("buzz");
            mockdivisibleby.Setup(p => p.GetOutput(It.Is<int>(i => i == 11))).Returns("11");
            mockdivisibleby.Setup(p => p.GetOutput(It.Is<int>(i => i == 12))).Returns("fizz");
            mockdivisibleby.Setup(p => p.GetOutput(It.Is<int>(i => i == 13))).Returns("13");
            mockdivisibleby.Setup(p => p.GetOutput(It.Is<int>(i => i == 14))).Returns("14");
            mockdivisibleby.Setup(p => p.GetOutput(It.Is<int>(i => i == 15))).Returns("fizzbuzz");

            var listofinstances = new List<IDivisible> {mockdivisibleby.Object};
            fizzbuzzservice = new FizzBuzzService(listofinstances);
        }

        [TestCase(15)]
        public void should_return_a_fizz_buzz_list(int number)
        {
            //Act
            var result = fizzbuzzservice.GetFizzBuzzList(number);

            //Assert
            Assert.AreEqual(result, finallist);
        }
    }
}
