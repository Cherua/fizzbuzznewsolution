﻿using System;

using BusinessLogic.InterfacesFizzBuzz;
using BusinessLogic.ClassesFizzBuzz;

using Moq;

using NUnit.Framework;

namespace FizzBuzzUnitTest
{
    [TestFixture]
    public class DivisibleByFiveUnitTest
    {
        private IDivisible divisiblebyfive;
        private Mock<ICheckForWednesday> mockday;

        [SetUp]
        public void LoadContext()
        {
            mockday = new Mock<ICheckForWednesday>();
            divisiblebyfive = new DivisibleByFive(mockday.Object);
        }

        [TestCase(5,ExpectedResult = true)]
        [TestCase(10, ExpectedResult = true)]
        [TestCase(15, ExpectedResult = true)]
        [TestCase(20, ExpectedResult = true)]
        [TestCase(25, ExpectedResult = true)]
        [TestCase(30, ExpectedResult = true)]
        [TestCase(35, ExpectedResult = true)]
        [TestCase(40, ExpectedResult = true)]
        [TestCase(45, ExpectedResult = true)]
        [TestCase(50, ExpectedResult = true)]
        public bool should_return_true_if_value_is_divisible_by_five(int number)
        {
            //Act
            var result = divisiblebyfive.Check(number);
            
            //Assert
            return result;
        }

        [TestCase(6, ExpectedResult = false)]
        [TestCase(11, ExpectedResult = false)]
        [TestCase(16, ExpectedResult = false)]
        [TestCase(21, ExpectedResult = false)]
        [TestCase(26, ExpectedResult = false)]
        [TestCase(31, ExpectedResult = false)]
        [TestCase(36, ExpectedResult = false)]
        [TestCase(41, ExpectedResult = false)]
        [TestCase(46, ExpectedResult = false)]
        [TestCase(51, ExpectedResult = false)]
        public bool should_return_false_if_value_is_divisible_by_five(int number)
        {
            //Act
            var result = divisiblebyfive.Check(number);

            //Assert
            return result;
        }
        
        [TestCase(5,ExpectedResult = "buzz")]
        [TestCase(10, ExpectedResult = "buzz")]
        [TestCase(15, ExpectedResult = "buzz")]
        [TestCase(20, ExpectedResult = "buzz")]
        [TestCase(25, ExpectedResult = "buzz")]
        [TestCase(30, ExpectedResult = "buzz")]
        [TestCase(35, ExpectedResult = "buzz")]
        [TestCase(40, ExpectedResult = "buzz")]
        [TestCase(45, ExpectedResult = "buzz")]
        [TestCase(50, ExpectedResult = "buzz")]
        public string should_return_buzz_if_divisible_by_five(int number)
        {
            //Arrange
            mockday.Setup(p => p.CheckToday(DateTime.Today.DayOfWeek)).Returns(false);

            //Act
            var result = divisiblebyfive.GetOutput(number);

            //Assert
            return result;
        }

        [TestCase(5, ExpectedResult = "wuzz")]
        [TestCase(10, ExpectedResult = "wuzz")]
        [TestCase(15, ExpectedResult = "wuzz")]
        [TestCase(20, ExpectedResult = "wuzz")]
        [TestCase(25, ExpectedResult = "wuzz")]
        [TestCase(30, ExpectedResult = "wuzz")]
        [TestCase(35, ExpectedResult = "wuzz")]
        [TestCase(40, ExpectedResult = "wuzz")]
        [TestCase(45, ExpectedResult = "wuzz")]
        [TestCase(50, ExpectedResult = "wuzz")]
        public string should_return_wuzz_if_divisible_by_five_and_today_is_wednesday(int number)
        {
            //Arrange
            mockday.Setup(p => p.CheckToday(DateTime.Today.DayOfWeek)).Returns(true);

            //Act
            var result = divisiblebyfive.GetOutput(number);

            //Assert
            return result;
        }

        [TestCase(6, ExpectedResult = null)]
        [TestCase(11, ExpectedResult = null)]
        [TestCase(16, ExpectedResult = null)]
        [TestCase(21, ExpectedResult = null)]
        [TestCase(26, ExpectedResult = null)]
        [TestCase(31, ExpectedResult = null)]
        [TestCase(36, ExpectedResult = null)]
        [TestCase(41, ExpectedResult = null)]
        [TestCase(46, ExpectedResult = null)]
        [TestCase(51, ExpectedResult = null)]
        public string should_return_null_if_not_divisible_by_five(int number)
        {
            //Arrange
            mockday.Setup(p => p.CheckToday(DateTime.Today.DayOfWeek)).Returns(false);

            //Act
            var result = divisiblebyfive.GetOutput(number);

            //Assert
            return result;
        }

        [TestCase(6, ExpectedResult = null)]
        [TestCase(11, ExpectedResult = null)]
        [TestCase(16, ExpectedResult = null)]
        [TestCase(21, ExpectedResult = null)]
        [TestCase(26, ExpectedResult = null)]
        [TestCase(31, ExpectedResult = null)]
        [TestCase(36, ExpectedResult = null)]
        [TestCase(41, ExpectedResult = null)]
        [TestCase(46, ExpectedResult = null)]
        [TestCase(51, ExpectedResult = null)]
        public string should_return_null_if_not_divisible_by_five_and_today_is_wednesday(int number)
        {
            //Arrange
            mockday.Setup(p => p.CheckToday(DateTime.Today.DayOfWeek)).Returns(true);

            //Act
            var result = divisiblebyfive.GetOutput(number);

            //Assert
            return result;
        }
    }
}
