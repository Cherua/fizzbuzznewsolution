﻿using System;

using BusinessLogic.InterfacesFizzBuzz;
using BusinessLogic.ClassesFizzBuzz;

using NUnit.Framework;

namespace FizzBuzzUnitTest
{
    [TestFixture]
    public class CheckForWednsdayUnitTest
    {
        private ICheckForWednesday daytoday;
        
        [SetUp]
        public void LoadContext()
        {
            daytoday = new CheckForWednesday();
        }

        [TestCase(DayOfWeek.Wednesday, ExpectedResult = true)] //Test Case-1
        public bool should_return_true_if_the_day_is_wednesday(DayOfWeek day)
        {
            //Act
            var result = daytoday.CheckToday(day);
            
            //Assert
            return result;
        }

        [TestCase(DayOfWeek.Monday, ExpectedResult = false)]   //Test Case-2
        [TestCase(DayOfWeek.Tuesday, ExpectedResult = false)]  //Test Case-3
        [TestCase(DayOfWeek.Thursday, ExpectedResult = false)] //Test Case-4
        [TestCase(DayOfWeek.Friday, ExpectedResult = false)]   //Test Case-5
        [TestCase(DayOfWeek.Saturday, ExpectedResult = false)] //Test Case-6
        [TestCase(DayOfWeek.Sunday, ExpectedResult = false)]   //Test Case-7
        public bool should_return_false_if_the_day_is_not_wednesday(DayOfWeek day)
        {
            //Act
            var result = daytoday.CheckToday(day);
            
            //Assert
            return result;
        }
    }
}
