﻿using System;

using BusinessLogic.InterfacesFizzBuzz;
using BusinessLogic.ClassesFizzBuzz;

using Moq;
using NUnit.Framework;

namespace FizzBuzzUnitTest
{
    [TestFixture]
    public class DivisibleByThreeUnitTest
    {
        private IDivisible divisiblebythree;
        private Mock<ICheckForWednesday> mockday;

        [SetUp]
        public void LoadContext()
        {
            mockday = new Mock<ICheckForWednesday>();
            divisiblebythree = new DivisibleByThree(mockday.Object);
        }

        [TestCase(3, ExpectedResult = true)]
        [TestCase(6, ExpectedResult = true)]
        [TestCase(9, ExpectedResult = true)]
        [TestCase(12, ExpectedResult = true)]
        [TestCase(15, ExpectedResult = true)]
        [TestCase(18, ExpectedResult = true)]
        [TestCase(21, ExpectedResult = true)]
        [TestCase(24, ExpectedResult = true)]
        [TestCase(27, ExpectedResult = true)]
        [TestCase(30, ExpectedResult = true)]
        public bool should_return_true_if_value_is_divisible_by_three(int number)
        {
            //Act
            var result = divisiblebythree.Check(number);

            //Assert
            return result;
        }

        [TestCase(1, ExpectedResult = false)]
        [TestCase(2, ExpectedResult = false)]
        [TestCase(4, ExpectedResult = false)]
        [TestCase(7, ExpectedResult = false)]
        [TestCase(8, ExpectedResult = false)]
        [TestCase(11, ExpectedResult = false)]
        [TestCase(13, ExpectedResult = false)]
        [TestCase(16, ExpectedResult = false)]
        [TestCase(17, ExpectedResult = false)]
        [TestCase(19, ExpectedResult = false)]
        public bool should_return_false_if_value_is_divisible_by_three(int number)
        {
            //Act
            var result = divisiblebythree.Check(number);

            //Assert
            return result;
        }

        [TestCase(3, ExpectedResult = "fizz")]
        [TestCase(6, ExpectedResult = "fizz")]
        [TestCase(9, ExpectedResult = "fizz")]
        [TestCase(12, ExpectedResult = "fizz")]
        [TestCase(15, ExpectedResult = "fizz")]
        [TestCase(18, ExpectedResult = "fizz")]
        [TestCase(21, ExpectedResult = "fizz")]
        [TestCase(24, ExpectedResult = "fizz")]
        [TestCase(27, ExpectedResult = "fizz")]
        [TestCase(30, ExpectedResult = "fizz")]
        public string should_return_fizz_if_divisible_by_three(int number)
        {
            //Arrange
            mockday.Setup(p => p.CheckToday(DateTime.Today.DayOfWeek)).Returns(false);

            //Act
            var result = divisiblebythree.GetOutput(number);

            //Assert
            return result;
        }

        [TestCase(3, ExpectedResult = "wizz")]
        [TestCase(6, ExpectedResult = "wizz")]
        [TestCase(9, ExpectedResult = "wizz")]
        [TestCase(12, ExpectedResult = "wizz")]
        [TestCase(15, ExpectedResult = "wizz")]
        [TestCase(18, ExpectedResult = "wizz")]
        [TestCase(21, ExpectedResult = "wizz")]
        [TestCase(24, ExpectedResult = "wizz")]
        [TestCase(27, ExpectedResult = "wizz")]
        [TestCase(30, ExpectedResult = "wizz")]
        public string should_return_wizz_if_divisible_by_three_and_today_is_wednesday(int number)
        {
            //Arrange
            mockday.Setup(p => p.CheckToday(DateTime.Today.DayOfWeek)).Returns(true);

            //Act
            var result = divisiblebythree.GetOutput(number);

            //Assert
            return result;
        }

        [TestCase(1, ExpectedResult = null)]
        [TestCase(2, ExpectedResult = null)]
        [TestCase(4, ExpectedResult = null)]
        [TestCase(5, ExpectedResult = null)]
        [TestCase(7, ExpectedResult = null)]
        [TestCase(8, ExpectedResult = null)]
        [TestCase(10, ExpectedResult = null)]
        [TestCase(11, ExpectedResult = null)]
        [TestCase(13, ExpectedResult = null)]
        [TestCase(14, ExpectedResult = null)]
        public string should_return_null_if_not_divisible_by_three(int number)
        {
            //Arrange
            mockday.Setup(p => p.CheckToday(DateTime.Today.DayOfWeek)).Returns(false);

            //Act
            var result = divisiblebythree.GetOutput(number);

            //Assert
            return result;
        }

        [TestCase(1, ExpectedResult = null)]
        [TestCase(2, ExpectedResult = null)]
        [TestCase(4, ExpectedResult = null)]
        [TestCase(5, ExpectedResult = null)]
        [TestCase(7, ExpectedResult = null)]
        [TestCase(8, ExpectedResult = null)]
        [TestCase(10, ExpectedResult = null)]
        [TestCase(11, ExpectedResult = null)]
        [TestCase(13, ExpectedResult = null)]
        [TestCase(14, ExpectedResult = null)]
        public string should_return_null_if_not_divisible_by_three_and_today_is_wednesday(int number)
        {
            //Arrange
            mockday.Setup(p => p.CheckToday(DateTime.Today.DayOfWeek)).Returns(true);

            //Act
            var result = divisiblebythree.GetOutput(number);

            //Assert
            return result;
        }
    }
}
