﻿using System.Collections.Generic;
using System.Web.Mvc;

using BusinessLogic.InterfacesFizzBuzz;
using FizzBuzzNewSolution.Models;
using FizzBuzzNewSolution.Controllers;

using Moq;
using NUnit.Framework;

using PagedList;

namespace FizzBuzzUnitTest
{
    [TestFixture]
    public class FizzBuzzControllerUnitTest
    {
        private FizzBuzzController fizzbuzzcontroller;
        private Mock<IFizzBuzzService> service;
        public FizzBuzzModel fizzbuzzmodel;
        List<string> fizzbuzzlist = new List<string>(new string[] {"1", "2", "fizz", "4", "buzz","fizz", "7", "8", "fizz", "buzz",
            "11", "fizz", "13", "14", "fizzbuzz" });

        [SetUp]
        public void LoadContext()
        {
            service = new Mock<IFizzBuzzService>();
            service.Setup(m => m.GetFizzBuzzList(It.IsAny<int>())).Returns(fizzbuzzlist);
            fizzbuzzcontroller = new FizzBuzzController(service.Object);
            fizzbuzzmodel = new FizzBuzzModel();
        }

        [TestCase]
        public void Should_return_index_actionresult_view_and_null_pagedlist_if_all_conditions_are_not_met()
        {
            //Arrange
            fizzbuzzmodel.userinput = -1;
            int pagenumber = 1;
            fizzbuzzcontroller.ModelState.AddModelError("Range", "Please enter a value within the range 1 to 1000");

            //Act
            var output = fizzbuzzcontroller.Output(fizzbuzzmodel, pagenumber) as ViewResult;

            //Assert
            Assert.AreEqual("Index",output.ViewName);
            Assert.IsNull(fizzbuzzmodel.results);
        }

        [TestCase]
        public void Should_return_a_pagedlist_if_all_conditions_are_met()
        {
            //Arrange
            fizzbuzzmodel.userinput = 15;
            int pagenumber = 1;
            int pagesize = 20;
            var pagedlist = fizzbuzzlist.ToPagedList(pagenumber, pagesize);

            //Act
            var output = fizzbuzzcontroller.Output(fizzbuzzmodel, pagenumber) as ViewResult;

            //Assert
            Assert.AreEqual("Index", output.ViewName);
            Assert.IsNotNull(fizzbuzzmodel.results);
            Assert.AreEqual(pagedlist.GetType(), fizzbuzzmodel.results.GetType());
            
        }
    }
}
