﻿

namespace BusinessLogic.InterfacesFizzBuzz
{
    public interface IDivisible
    {
        bool Check(int number);
        string GetOutput(int number);
    }
}
