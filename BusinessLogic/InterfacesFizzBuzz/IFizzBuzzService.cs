﻿using System.Collections.Generic;


namespace BusinessLogic.InterfacesFizzBuzz
{
    public interface IFizzBuzzService
    {
        List<string> GetFizzBuzzList(int number);
    }
}
