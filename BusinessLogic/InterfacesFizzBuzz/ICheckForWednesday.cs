﻿using System;

namespace BusinessLogic.InterfacesFizzBuzz
{
    public interface ICheckForWednesday
    {
        bool CheckToday(DayOfWeek daytoday);
    }
}
