﻿using System;
using BusinessLogic.InterfacesFizzBuzz;

namespace BusinessLogic.ClassesFizzBuzz
{
    public class CheckForWednesday : ICheckForWednesday
    {
        public bool CheckToday(DayOfWeek daytoday)
        {
            return daytoday == DayOfWeek.Wednesday;
        }
    }
}
