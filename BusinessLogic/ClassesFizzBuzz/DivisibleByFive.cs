﻿using System;
using BusinessLogic.InterfacesFizzBuzz;

namespace BusinessLogic.ClassesFizzBuzz
{
    public class DivisibleByFive : IDivisible
    {
        ICheckForWednesday daytoday;
        public DivisibleByFive(ICheckForWednesday daytoday)
        {
            this.daytoday = daytoday;
        }
        public bool Check(int number)
        {
            return number % 5 == 0;
        }
        public string GetOutput(int number)
        {
            if (Check(number))
            {
                if (this.daytoday.CheckToday(DateTime.Today.DayOfWeek))
                {
                    return "wuzz";
                }
                else
                {
                    return "buzz";
                }
            }
            else
            {
                return null;
            }
        }
    }
}
