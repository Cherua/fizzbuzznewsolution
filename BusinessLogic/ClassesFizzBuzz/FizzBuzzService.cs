﻿using System.Collections.Generic;
using System.Linq;
using BusinessLogic.InterfacesFizzBuzz;

namespace BusinessLogic.ClassesFizzBuzz
{
    public class FizzBuzzService : IFizzBuzzService
    {
        private IList<IDivisible> rules;
        public FizzBuzzService(IList<IDivisible> rules)
        {
            this.rules = rules;
        }
        public List<string> GetFizzBuzzList(int number)
        {
            var fizzbuzzlist = new List<string>();
            var finalresult = "";

            for (int i = 1; i<= number;i++)
            {
                var matchedrules = this.rules.Where(p => p.Check(i));
                if (matchedrules.Any())
                {
                    var result = matchedrules.Select(p => p.GetOutput(i));
                    finalresult = string.Join("", result);
                }
                else
                {
                    finalresult = i.ToString();
                }
                fizzbuzzlist.Add(finalresult);

            }
            return fizzbuzzlist;
        }
    }
}
