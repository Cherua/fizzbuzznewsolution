﻿using System;
using BusinessLogic.InterfacesFizzBuzz;

namespace BusinessLogic.ClassesFizzBuzz
{
    public class DivisibleByThree : IDivisible
    {
        ICheckForWednesday daytoday;
        public DivisibleByThree(ICheckForWednesday daytoday)
        {
            this.daytoday = daytoday;
        }
        public bool Check(int number)
        {
            return number % 3 == 0;
        }
        public string GetOutput(int number)
        {
            if (Check(number))
            {
                if (this.daytoday.CheckToday(DateTime.Today.DayOfWeek))
                {
                    return "wizz";
                }
                else
                {
                    return "fizz";
                }
            }
            else
            {
                return null;
            }
        }
    }
}
