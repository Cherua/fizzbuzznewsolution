﻿using System.ComponentModel.DataAnnotations;
using PagedList;


namespace FizzBuzzNewSolution.Models
{
    public class FizzBuzzModel
    {

        [Required(ErrorMessage = "Please enter a valid value")]
        [Range(1, 1000, ErrorMessage = "Please enter a value within the range 1 to 1000")]
        public int userinput { get; set; }

        public IPagedList<string> results { get; set; }
    }
}