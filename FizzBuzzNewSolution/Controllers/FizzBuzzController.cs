﻿using System.Web.Mvc;
using FizzBuzzNewSolution.Models;
using BusinessLogic.InterfacesFizzBuzz;
using PagedList;

namespace FizzBuzzNewSolution.Controllers
{
    public class FizzBuzzController : Controller
    {
        private readonly IFizzBuzzService fizzbuzzservice;
        public FizzBuzzController(IFizzBuzzService fizzbuzzservice)
        {
            this.fizzbuzzservice = fizzbuzzservice;
        }

        // GET: FizzBuzz
        [HttpGet]
        public ActionResult Index()
        {
            return View(new FizzBuzzModel());
        }

        [HttpGet]
        public ActionResult Output(FizzBuzzModel fizzbuzzmodel, int? page)
        {
            if (!ModelState.IsValid)
            {
                return View("Index", fizzbuzzmodel);
            }
            var finaloutput = this.fizzbuzzservice.GetFizzBuzzList(fizzbuzzmodel.userinput);
            fizzbuzzmodel.results = finaloutput.ToPagedList(page ?? 1, 20);
            return View("Index",fizzbuzzmodel); 
        }
    }  
}